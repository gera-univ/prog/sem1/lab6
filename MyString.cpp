//
// Created by herman on 11/8/19.
//

#include "MyString.h"

char* MyString::strcpy(char *strDestination, const char *strSource) {
    char *ptrDestination = strDestination;
    while((*strDestination++ = *strSource++));
    *strDestination = '\0';
    return ptrDestination;
}

int MyString::findAscendingWord(const char *str) {
    int wordStart = 0;
    int index = 0;
    bool word = false;
    bool ascending = true;
    if (*str == '\0' || *str == ' ') return -1;
    while (*str) {
        if (*str == ' ') {
            if (word && ascending) {
                return wordStart;
            }
            word = false;
            ascending = true;
        } else {
            if (!word) {
                wordStart = index;
                word = true;
            }
            if (*(str - 1) > *str && *(str - 1) != ' ' && index > 0)
                ascending = false;
        }
        ++index;
        ++str;
    }
    return (ascending) ? wordStart : -1;
}

char *MyString::itoa (int value, char *string, int radix) {
    char *ptrString = string;
    bool isNegative = false;
    if (value < 0) {
        isNegative = true;
        value *= -1;
    } else if (value == 0) {
        *string = '0';
        ++string;
    }
        while (value > 0) {
        int r = value % radix;
        value /= radix;
        *string = digitToChar(r);
        ++string;
    }
    char *ptrReverseString = string;
    char *beginReverseString = ptrReverseString;
    if (isNegative)
        *beginReverseString = '-';
    else ++beginReverseString;
    while (string >= ptrString) {
        *ptrReverseString = *string;
        ++ptrReverseString;
        --string;
    }
    *ptrReverseString = '\0';
    return beginReverseString;
}

char MyString::digitToChar(char digit) {
    if (digit > 36 || digit < 0) throw "Incorrect digit!";
    return (digit > 9) ? digit - 10 + 'A' : '0' + digit;
}

int MyString::countWordChars(const char *str) {
    int counter = 0;
    char *dict = new char[DICT_MAX_SIZE];
    dict[0] = '\0';
    while (*str && *str != ' ') {
        char *ptrDict = dict;
        bool charInDict = false;
        while (*ptrDict) {
            if (*ptrDict == *str) {
                charInDict = true;
                break;
            }
            ++ptrDict;
        }
        if(!charInDict) {
            *ptrDict = *str;
            ++ptrDict;
            *ptrDict = '\0';
            ++counter;
        }
        ++str;
    }
    delete [] dict;
    return counter;
}

int MyString::findLastWordWithMaxChars(const char *str) {
    int wordStart = 0;
    int index = 0;
    bool word = false;
    int maxChars = 0;
    int maxCharsIndex = 0;
    int curChars = 0;
    if (*str == '\0' || *str == ' ') return -1;
    while (*str) {
        if (*str == ' ' || !*(str+1)) {
            word = false;
            if (curChars >= maxChars) {
                maxChars = curChars;
                maxCharsIndex = wordStart;
            }
        } else if (!word) {
            wordStart = index;
            word = true;

            curChars = countWordChars(str);
        }
        ++index;
        ++str;
    }
    return maxCharsIndex;
}
