//
// Created by herman on 11/7/19.
//

#ifndef LAB6_MYSTRING_H
#define LAB6_MYSTRING_H

namespace MyString {
    const int DICT_MAX_SIZE = 1024;

    // вариант 4
    char *strcpy(char *strDestination, const char *strSource);

    int findAscendingWord(const char *str);

    // вариант 5
    char *itoa(int value, char *string, int radix);

    int findLastWordWithMaxChars(const char *str);

    // вспомогательные функции
    char digitToChar(char digit);

    int countWordChars(const char *str);
}

#endif //LAB6_MYSTRING_H
