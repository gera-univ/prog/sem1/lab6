#include <cstdio>

#include "MyString.h"

int main() {
    const size_t MAX_SIZE = 100000;
    char str[MAX_SIZE];
    int number, base;
    printf("Enter decimal integer and base to convert it to\n");
    scanf("%d %d", &number, &base);
    if (base < 2 || base > 36) {
        printf("Error: base should be in range between 2 and 36!\n");
        return -1;
    }
    printf("testing MyString::itoa:\n");
    printf("%s",MyString::itoa(number, str, base));
    return 0;
}