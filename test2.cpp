#include <cstdio>

#include "MyString.h"

void printToSpace(char *str) {
    while (*str && *str != ' ')
        printf("%c",*str++);
    printf("\n");
}
int main() {
    const size_t MAX_SIZE = 100000;
    char str[MAX_SIZE];
    char *ptr = str;
    printf("Enter string\n");
    while ((*ptr = getchar()) != '\n') {++ptr;}
    printf("testing MyString::findLastWordWithMaxChars:\n");
    printToSpace(str+MyString::findLastWordWithMaxChars(str));
    return 0;
}